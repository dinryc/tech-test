﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageResources.Business.Entities
{
    public class AcquisitionItem:EntityBase
    {        
        public long AcquisitionId { get; set; }
        public long ResourceId { get; set; }
        public double Quantity { get; set; }
        public virtual Acquisition Acquisition { get; set; }
        public virtual Resource Resource { get; set; }
    }
}
