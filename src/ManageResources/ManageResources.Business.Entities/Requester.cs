﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageResources.Business.Entities
{
    public class Requester: EntityBase
    {        
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
        public virtual ICollection<Acquisition> Acquisitions { get; set; }
        public virtual ICollection<Dispatch> Dipatches { get; set; }
    }
}
