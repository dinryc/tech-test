﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageResources.Business.Entities
{
    public class Dispatch:EntityBase
    {        
        public long RequesterId { get; set; }
        public DateTime DateTime { get; set; }
        public virtual Requester Requester { get; set; }
        public virtual ICollection<DispatchItem> DispatchItems { get; set; }
    }
}
