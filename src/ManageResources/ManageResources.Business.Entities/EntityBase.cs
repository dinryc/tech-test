﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageResources.Business.Entities
{
    public abstract class EntityBase
    {
        public long Id { get; set; }
        public bool Active { get; set; }
    }
}
