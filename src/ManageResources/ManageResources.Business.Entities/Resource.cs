﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageResources.Business.Entities
{
    public class Resource:EntityBase
    {        
        public string Description { get; set; }
        public double Quantity { get; set; }
        public string Note { get; set; }
        public virtual ICollection<AcquisitionItem> AcquisitionItems { get; set; }
        public virtual ICollection<DispatchItem> DispatchItems { get; set; }
    }
}
