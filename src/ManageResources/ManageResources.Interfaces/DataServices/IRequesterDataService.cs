﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManageResources.Business.Entities;

namespace ManageResources.Interfaces.DataServices
{
    public interface IRequesterDataService : IDataRepository<Requester>, IDisposable
    {
    }
}
