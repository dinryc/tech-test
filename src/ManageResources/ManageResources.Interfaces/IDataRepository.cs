﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManageResources.Business.Entities;

namespace ManageResources.Interfaces
{
    public interface IDataRepository<TEntity> where TEntity : EntityBase
    {
        //Relacionado a Transação
        void CreateSession();        
        void CommitTransaction(Boolean closeSession);
        //Relacionado à Entidade
        void Create(TEntity Object);
        void Update(TEntity Object);
        TEntity GetById(long id);
        IEnumerable<TEntity> GetList();
        void Remove(TEntity Object);
    }
}
