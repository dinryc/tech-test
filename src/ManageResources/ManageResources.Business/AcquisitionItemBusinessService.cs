﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManageResources.Business.Entities;
using ManageResources.Interfaces.DataServices;

namespace ManageResources.Business
{
    public class AcquisitionItemBusinessService
    {
        private IAcquisitionItemDataService _acquisitionItemDataService;

        public AcquisitionItemBusinessService(IAcquisitionItemDataService acquisitionItemDataService)
        {
            _acquisitionItemDataService = acquisitionItemDataService;
        }

        public AcquisitionItem CreateAcquisitionItem(AcquisitionItem acquisitionItem)
        {
            acquisitionItem.Active = true;
            try
            {
                _acquisitionItemDataService.CreateSession();
                _acquisitionItemDataService.Create(acquisitionItem);
                _acquisitionItemDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
                //TO DO: implement return object                
            }

            return acquisitionItem;
        }

        public List<AcquisitionItem> GetAcquisitionItems()
        {
            List<AcquisitionItem> acquisitionItems = new List<AcquisitionItem>();
            try
            {
                _acquisitionItemDataService.CreateSession();
                acquisitionItems = _acquisitionItemDataService.GetList().ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

            return acquisitionItems;
        }

        public AcquisitionItem GetById(long Id)
        {
            AcquisitionItem acquisitionItem = new AcquisitionItem();

            try
            {
                _acquisitionItemDataService.CreateSession();
                acquisitionItem = _acquisitionItemDataService.GetById(Id);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

            return acquisitionItem;
        }
        public void UpdateAcquisitionItem(AcquisitionItem acquisitionItem)
        {
            acquisitionItem.Active = true;
            try
            {
                _acquisitionItemDataService.CreateSession();
                _acquisitionItemDataService.Update(acquisitionItem);
                _acquisitionItemDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

        }

        public void RemoveAcquisitionItem(AcquisitionItem acquisitionItem)
        {
            acquisitionItem.Active = false;
            try
            {
                _acquisitionItemDataService.CreateSession();
                _acquisitionItemDataService.Update(acquisitionItem);
                _acquisitionItemDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
        }
    }
}
