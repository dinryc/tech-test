﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManageResources.Business.Entities;
using ManageResources.Interfaces.DataServices;

namespace ManageResources.Business
{
    public class AcquisitionBusinessService
    {
        private IAcquisitionDataService _acquisitionDataService;

        public AcquisitionBusinessService(IAcquisitionDataService acquisitionDataService)
        {
            _acquisitionDataService = acquisitionDataService;
        }

        public Acquisition CreateAcquisition(Acquisition acquisition)
        {
            acquisition.Active = true;
            try
            {
                _acquisitionDataService.CreateSession();
                _acquisitionDataService.Create(acquisition);
                _acquisitionDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
                //TO DO: implement return object                
            }

            return acquisition;
        }

        public List<Acquisition> GetAcquisitions()
        {
            List<Acquisition> acquisitions = new List<Acquisition>();
            try
            {
                _acquisitionDataService.CreateSession();
                acquisitions = _acquisitionDataService.GetList().ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

            return acquisitions;
        }

        public Acquisition GetById(long Id)
        {
            Acquisition acquisition = new Acquisition();

            try
            {
                _acquisitionDataService.CreateSession();
                acquisition = _acquisitionDataService.GetById(Id);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

            return acquisition;
        }
        public void UpdateAcquisition(Acquisition acquisition)
        {
            acquisition.Active = true;
            try
            {
                _acquisitionDataService.CreateSession();
                _acquisitionDataService.Update(acquisition);
                _acquisitionDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

        }

        public void RemoveAcquisition(Acquisition acquisition)
        {
            acquisition.Active = false;
            try
            {
                _acquisitionDataService.CreateSession();
                _acquisitionDataService.Update(acquisition);
                _acquisitionDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
        }
    }
}
