﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManageResources.Business.Entities;
using FluentValidation;

namespace ManageResources.Business
{
    public class ResourceBusinessRule : AbstractValidator<Resource>
    {
        public ResourceBusinessRule()
        {
            RuleFor(r => r.Description).NotEmpty().WithMessage("Descrição do recurso é obrigatória!");
        }
    }
}
