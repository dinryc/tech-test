﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManageResources.Business.Entities;
using ManageResources.Interfaces.DataServices;

namespace ManageResources.Business
{
    public class DispatchBusinessService
    {
        private IDispatchDataService _dispatchDataService;

        public DispatchBusinessService(IDispatchDataService dispatchDataService)
        {
            _dispatchDataService = dispatchDataService;
        }

        public Dispatch CreateDispatch(Dispatch dispatch)
        {
            dispatch.Active = true;
            try
            {
                _dispatchDataService.CreateSession();
                _dispatchDataService.Create(dispatch);
                _dispatchDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
                //TO DO: implement return object                
            }

            return dispatch;
        }

        public List<Dispatch> GetDispatches()
        {
            List<Dispatch> dispatches = new List<Dispatch>();
            try
            {
                _dispatchDataService.CreateSession();
                dispatches = _dispatchDataService.GetList().ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

            return dispatches;
        }

        public Dispatch GetById(long Id)
        {
            Dispatch dispatch = new Dispatch();

            try
            {
                _dispatchDataService.CreateSession();
                dispatch = _dispatchDataService.GetById(Id);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

            return dispatch;
        }
        public void UpdateDispatch(Dispatch dispatch)
        {
            dispatch.Active = true;
            try
            {
                _dispatchDataService.CreateSession();
                _dispatchDataService.Update(dispatch);
                _dispatchDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

        }

        public void RemoveDispatch(Dispatch dispatch)
        {
            dispatch.Active = false;
            try
            {
                _dispatchDataService.CreateSession();
                _dispatchDataService.Update(dispatch);
                _dispatchDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
        }
    }
}
