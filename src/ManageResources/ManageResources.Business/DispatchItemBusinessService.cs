﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManageResources.Business.Entities;
using ManageResources.Interfaces.DataServices;

namespace ManageResources.Business
{
    public class DispatchItemBusinessService
    {
        private IDispatchItemDataService _dispatchItemDataService;

        public DispatchItemBusinessService(IDispatchItemDataService dispatchItemDataService)
        {
            _dispatchItemDataService = dispatchItemDataService;
        }

        public DispatchItem CreateDispatchItem(DispatchItem dispatchItem)
        {
            dispatchItem.Active = true;
            try
            {
                _dispatchItemDataService.CreateSession();
                _dispatchItemDataService.Create(dispatchItem);
                _dispatchItemDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
                //TO DO: implement return object                
            }

            return dispatchItem;
        }

        public List<DispatchItem> GetDispatchItems()
        {
            List<DispatchItem> dispatchItems = new List<DispatchItem>();
            try
            {
                _dispatchItemDataService.CreateSession();
                dispatchItems = _dispatchItemDataService.GetList().ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

            return dispatchItems;
        }

        public DispatchItem GetById(long Id)
        {
            DispatchItem dispatchItem = new DispatchItem();

            try
            {
                _dispatchItemDataService.CreateSession();
                dispatchItem = _dispatchItemDataService.GetById(Id);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

            return dispatchItem;
        }
        public void UpdateDispatchItem(DispatchItem dispatchItem)
        {
            dispatchItem.Active = true;
            try
            {
                _dispatchItemDataService.CreateSession();
                _dispatchItemDataService.Update(dispatchItem);
                _dispatchItemDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

        }

        public void RemoveDispatchItem(DispatchItem dispatchItem)
        {
            dispatchItem.Active = false;
            try
            {
                _dispatchItemDataService.CreateSession();
                _dispatchItemDataService.Update(dispatchItem);
                _dispatchItemDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
        }
    }
}
