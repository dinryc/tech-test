﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManageResources.Business.Entities;
using ManageResources.Interfaces.DataServices;

namespace ManageResources.Business
{
    public class RequesterBusinessService
    {
        private IRequesterDataService _requesterDataService;

        public RequesterBusinessService(IRequesterDataService requesterDataService)
        {
            _requesterDataService = requesterDataService;
        }

        public Requester CreateRequester(Requester requester)
        {
            requester.Active = true;
            try
            {
                _requesterDataService.CreateSession();
                _requesterDataService.Create(requester);
                _requesterDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
                //TO DO: implement return object                
            }

            return requester;
        }

        public List<Requester> GetRequesters()
        {
            List<Requester> requesters = new List<Requester>();
            try
            {
                _requesterDataService.CreateSession();
                requesters = _requesterDataService.GetList().ToList();
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

            return requesters;
        }

        public Requester GetById(long Id)
        {
            Requester requester = new Requester();

            try
            {
                _requesterDataService.CreateSession();
                requester = _requesterDataService.GetById(Id);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

            return requester;
        }
        public void UpdateRequester(Requester requester)
        {
            requester.Active = true;
            try
            {
                _requesterDataService.CreateSession();
                _requesterDataService.Update(requester);
                _requesterDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }

        }

        public void RemoveRequester(Requester requester)
        {
            requester.Active = false;
            try
            {
                _requesterDataService.CreateSession();
                _requesterDataService.Update(requester);
                _requesterDataService.CommitTransaction(true);
            }
            catch (Exception e)
            {
                string error = e.Message;
            }
        }
    }
}
