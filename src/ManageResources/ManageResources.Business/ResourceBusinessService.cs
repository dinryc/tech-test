﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ManageResources.Business.Entities;
using ManageResources.Business.Common;
using ManageResources.Interfaces.DataServices;
using FluentValidation.Results;

namespace ManageResources.Business
{
    public class ResourceBusinessService
    {
        private IResourceDataService _resourceDataService;
        private TransactionInfo _transaction;
        public ResourceBusinessService(IResourceDataService resourceDataService)
        {
            _resourceDataService = resourceDataService;
            _transaction = new TransactionInfo();
        }

        public Resource CreateResource(Resource resource, out TransactionInfo transaction)
        {
            transaction = _transaction;
            resource.Active = true;
            try
            {
                ResourceBusinessRule resourceBusinessRule = new ResourceBusinessRule();
                ValidationResult results = resourceBusinessRule.Validate(resource);

                bool validated = results.IsValid;
                IList<ValidationFailure> failures = results.Errors;

                if (!validated)
                {
                    transaction = ValidationErrors.PopulateValidationErrors(failures);
                    return resource;
                }

                _resourceDataService.CreateSession();
                _resourceDataService.Create(resource);
                _resourceDataService.CommitTransaction(true);

                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Recurso criado com sucesso!");
            }
            catch (Exception e)
            {
                string error = e.Message;
                transaction.ReturnMessage.Add(error);
                transaction.ReturnStatus = false;
            }

            return resource;
        }

        public List<Resource> GetResources(out TransactionInfo transaction)
        {
            transaction = _transaction;
            List<Resource> resources = new List<Resource>();
            try
            {
                _resourceDataService.CreateSession();
                resources = _resourceDataService.GetList().ToList();
                transaction.ReturnStatus = true;
            }
            catch (Exception e)
            {
                string error = e.Message;
                transaction.ReturnMessage.Add(error);
                transaction.ReturnStatus = false;
            }

            return resources;
        }

        public Resource GetById(long Id, out TransactionInfo transaction)
        {
            transaction = _transaction;
            Resource resource = new Resource();
            try
            {
                _resourceDataService.CreateSession();
                resource = _resourceDataService.GetById(Id);
                transaction.ReturnStatus = true;
            }
            catch (Exception e)
            {
                string error = e.Message;
                transaction.ReturnMessage.Add(error);
                transaction.ReturnStatus = false;
            }

            return resource;
        }
        public void UpdateResource(Resource resource, out TransactionInfo transaction)
        {
            transaction = _transaction;
            resource.Active = true;
            try
            {
                ResourceBusinessRule resourceBusinessRule = new ResourceBusinessRule();
                ValidationResult results = resourceBusinessRule.Validate(resource);

                bool validated = results.IsValid;
                IList<ValidationFailure> failures = results.Errors;

                if (!validated)
                {
                    transaction = ValidationErrors.PopulateValidationErrors(failures);
                    return;
                }

                _resourceDataService.CreateSession();
                if (_resourceDataService.GetById(resource.Id) == null)
                {
                    transaction.ReturnMessage.Add("Recurso não encontra-se removido ou não existe!");
                    transaction.ReturnStatus = true;
                    return;
                }
                _resourceDataService.Dispose();

                _resourceDataService.CreateSession();
                _resourceDataService.Update(resource);
                _resourceDataService.CommitTransaction(true);

                transaction.ReturnMessage.Add("Recurso alterado com sucesso!");
                transaction.ReturnStatus = true;
            }
            catch (Exception e)
            {
                string error = e.Message;
                transaction.ReturnMessage.Add(error);
                transaction.ReturnStatus = false;
            }

        }

        public void RemoveResource(long id, out TransactionInfo transaction)
        {
            transaction = _transaction;
            Resource resource = new Resource();
            try
            {
                _resourceDataService.CreateSession();

                resource =_resourceDataService.GetById(id);
                resource.Active = false;

                _resourceDataService.Update(resource);
                _resourceDataService.CommitTransaction(true);
                transaction.ReturnStatus = true;
                transaction.ReturnMessage.Add("Recurso removido com sucesso!");
            }
            catch (Exception e)
            {
                string error = e.Message;
                transaction.ReturnMessage.Add(error);
                transaction.ReturnStatus = false;
            }
        }
    }
}
