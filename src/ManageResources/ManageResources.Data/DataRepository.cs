﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ManageResources.Business.Entities;
using ManageResources.Interfaces;

namespace ManageResources.Data
{
    public class DataRepository<TEntity> : IDisposable, IDataRepository<TEntity> where TEntity: EntityBase
    {
        protected ManageResourcesContext _conn;

        public ManageResourcesContext dbConnection
        {
            get { return _conn; }
        }

        public void CommitTransaction(bool closeSession)
        {
            dbConnection.SaveChanges();
        }

        public void Create(TEntity Object)
        {
            dbConnection.Set<TEntity>().Add(Object);
        }

        public void CreateSession()
        {
            _conn = new ManageResourcesContext();
        }

        public void Dispose()
        {
            if (_conn != null)
                _conn.Dispose();
        }

        public TEntity GetById(long id)
        {
            return dbConnection.Set<TEntity>().Where(e => e.Id == id && e.Active == true).FirstOrDefault();
        }

        public IEnumerable<TEntity> GetList()
        {
            return dbConnection.Set<TEntity>().Where(e => e.Active == true).ToList();
        }

        public void Remove(TEntity Object)
        {
            dbConnection.Entry(Object).State = EntityState.Modified;
        }

        public void Update(TEntity Object)
        {
            dbConnection.Entry(Object).State = EntityState.Modified;
        }
    }
}
