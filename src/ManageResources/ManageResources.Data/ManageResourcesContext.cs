﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ManageResources.Business.Entities;

namespace ManageResources.Data
{
    public class ManageResourcesContext : DbContext
    {
        public ManageResourcesContext()
            :base("ManageResources")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<Resource> Resource { get; set; }
        public DbSet<Requester> Requester { get; set; }
        public DbSet<Acquisition> Acquisition { get; set; }
        public DbSet<AcquisitionItem> AcquisitionItem { get; set; }
        public DbSet<Dispatch> Dispatch { get; set; }
        public DbSet<DispatchItem> DispatchItem { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(200));

            modelBuilder.Entity<Resource>()
                .HasMany(r => r.AcquisitionItems)
                .WithRequired()
                .HasForeignKey(a => a.ResourceId);

            modelBuilder.Entity<Resource>()
                .HasMany(r => r.DispatchItems)
                .WithRequired()
                .HasForeignKey(d => d.ResourceId);

            modelBuilder.Entity<Requester>()
                .HasMany(r => r.Acquisitions)
                .WithRequired()
                .HasForeignKey(a => a.RequesterId);

            modelBuilder.Entity<Requester>()
                .HasMany(r => r.Dipatches)
                .WithRequired()
                .HasForeignKey(d => d.RequesterId);

            modelBuilder.Entity<Acquisition>()
                .HasMany(a => a.AcquisitionItems)
                .WithRequired()
                .HasForeignKey(ai => ai.AcquisitionId);

            modelBuilder.Entity<Dispatch>()                
                .HasMany(d => d.DispatchItems)
                .WithRequired()
                .HasForeignKey(di => di.DispatchId);

            modelBuilder.Entity<AcquisitionItem>();
            modelBuilder.Entity<DispatchItem>();
        }
    }
}
