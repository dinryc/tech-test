namespace ManageResources.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPropertyQuantityAcquistionItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AcquisitionItem", "Quantity", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AcquisitionItem", "Quantity");
        }
    }
}
