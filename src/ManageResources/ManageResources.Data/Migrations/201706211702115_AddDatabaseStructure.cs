namespace ManageResources.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDatabaseStructure : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Acquisition",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RequesterId = c.Long(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Requester", t => t.RequesterId)
                .Index(t => t.RequesterId);
            
            CreateTable(
                "dbo.AcquisitionItem",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AcquisitionId = c.Long(nullable: false),
                        ResourceId = c.Long(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Acquisition", t => t.AcquisitionId)
                .ForeignKey("dbo.Resource", t => t.ResourceId)
                .Index(t => t.AcquisitionId)
                .Index(t => t.ResourceId);
            
            CreateTable(
                "dbo.Resource",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Description = c.String(maxLength: 200, unicode: false),
                        Quantity = c.Double(nullable: false),
                        Note = c.String(maxLength: 200, unicode: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DispatchItem",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DispatchId = c.Long(nullable: false),
                        ResourceId = c.Long(nullable: false),
                        Quantity = c.Double(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Dispatch", t => t.DispatchId)
                .ForeignKey("dbo.Resource", t => t.ResourceId)
                .Index(t => t.DispatchId)
                .Index(t => t.ResourceId);
            
            CreateTable(
                "dbo.Dispatch",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RequesterId = c.Long(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Requester", t => t.RequesterId)
                .Index(t => t.RequesterId);
            
            CreateTable(
                "dbo.Requester",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Nome = c.String(maxLength: 200, unicode: false),
                        Sobrenome = c.String(maxLength: 200, unicode: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DispatchItem", "ResourceId", "dbo.Resource");
            DropForeignKey("dbo.Dispatch", "RequesterId", "dbo.Requester");
            DropForeignKey("dbo.Acquisition", "RequesterId", "dbo.Requester");
            DropForeignKey("dbo.DispatchItem", "DispatchId", "dbo.Dispatch");
            DropForeignKey("dbo.AcquisitionItem", "ResourceId", "dbo.Resource");
            DropForeignKey("dbo.AcquisitionItem", "AcquisitionId", "dbo.Acquisition");
            DropIndex("dbo.Dispatch", new[] { "RequesterId" });
            DropIndex("dbo.DispatchItem", new[] { "ResourceId" });
            DropIndex("dbo.DispatchItem", new[] { "DispatchId" });
            DropIndex("dbo.AcquisitionItem", new[] { "ResourceId" });
            DropIndex("dbo.AcquisitionItem", new[] { "AcquisitionId" });
            DropIndex("dbo.Acquisition", new[] { "RequesterId" });
            DropTable("dbo.Requester");
            DropTable("dbo.Dispatch");
            DropTable("dbo.DispatchItem");
            DropTable("dbo.Resource");
            DropTable("dbo.AcquisitionItem");
            DropTable("dbo.Acquisition");
        }
    }
}
