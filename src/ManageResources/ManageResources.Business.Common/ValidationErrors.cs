﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using ManageResources.Business.Entities;


namespace ManageResources.Business.Common
{
    public static class ValidationErrors
    {
        public static TransactionInfo PopulateValidationErrors(IList<ValidationFailure> failures)
        {
            TransactionInfo transaction = new TransactionInfo();

            transaction.ReturnStatus = false;
            foreach (ValidationFailure error in failures)
            {
                if (transaction.ValidationErrors.ContainsKey(error.PropertyName) == false)
                    transaction.ValidationErrors.Add(error.PropertyName, error.ErrorMessage);

                transaction.ReturnMessage.Add(error.ErrorMessage);
            }

            return transaction;
        }
    }
}
