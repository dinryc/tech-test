﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ManageResources.Business.Entities;
using ManageResources.Data;

namespace ManageResources.API.Controllers
{
    public class DispatchesController : Controller
    {
        private ManageResourcesContext db = new ManageResourcesContext();

        // GET: Dispatches
        public ActionResult Index()
        {
            var dispatch = db.Dispatch.Include(d => d.Requester);
            return View(dispatch.ToList());
        }

        // GET: Dispatches/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dispatch dispatch = db.Dispatch.Find(id);
            if (dispatch == null)
            {
                return HttpNotFound();
            }
            return View(dispatch);
        }

        // GET: Dispatches/Create
        public ActionResult Create()
        {
            ViewBag.RequesterId = new SelectList(db.Requester, "Id", "Nome");
            return View();
        }

        // POST: Dispatches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,RequesterId,DateTime,Active")] Dispatch dispatch)
        {
            if (ModelState.IsValid)
            {
                db.Dispatch.Add(dispatch);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RequesterId = new SelectList(db.Requester, "Id", "Nome", dispatch.RequesterId);
            return View(dispatch);
        }

        // GET: Dispatches/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dispatch dispatch = db.Dispatch.Find(id);
            if (dispatch == null)
            {
                return HttpNotFound();
            }
            ViewBag.RequesterId = new SelectList(db.Requester, "Id", "Nome", dispatch.RequesterId);
            return View(dispatch);
        }

        // POST: Dispatches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RequesterId,DateTime,Active")] Dispatch dispatch)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dispatch).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RequesterId = new SelectList(db.Requester, "Id", "Nome", dispatch.RequesterId);
            return View(dispatch);
        }

        // GET: Dispatches/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dispatch dispatch = db.Dispatch.Find(id);
            if (dispatch == null)
            {
                return HttpNotFound();
            }
            return View(dispatch);
        }

        // POST: Dispatches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Dispatch dispatch = db.Dispatch.Find(id);
            db.Dispatch.Remove(dispatch);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
