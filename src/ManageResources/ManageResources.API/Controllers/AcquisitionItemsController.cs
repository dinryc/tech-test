﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ManageResources.Business.Entities;
using ManageResources.Data;

namespace ManageResources.API.Controllers
{
    public class AcquisitionItemsController : Controller
    {
        private ManageResourcesContext db = new ManageResourcesContext();

        // GET: AcquisitionItems
        public ActionResult Index()
        {
            var acquisitionItem = db.AcquisitionItem.Include(a => a.Acquisition).Include(a => a.Resource);
            return View(acquisitionItem.ToList());
        }

        // GET: AcquisitionItems/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AcquisitionItem acquisitionItem = db.AcquisitionItem.Find(id);
            if (acquisitionItem == null)
            {
                return HttpNotFound();
            }
            return View(acquisitionItem);
        }

        // GET: AcquisitionItems/Create
        public ActionResult Create()
        {
            ViewBag.AcquisitionId = new SelectList(db.Acquisition, "Id", "Id");
            ViewBag.ResourceId = new SelectList(db.Resource, "Id", "Description");
            return View();
        }

        // POST: AcquisitionItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,AcquisitionId,ResourceId,Active")] AcquisitionItem acquisitionItem)
        {
            if (ModelState.IsValid)
            {
                db.AcquisitionItem.Add(acquisitionItem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AcquisitionId = new SelectList(db.Acquisition, "Id", "Id", acquisitionItem.AcquisitionId);
            ViewBag.ResourceId = new SelectList(db.Resource, "Id", "Description", acquisitionItem.ResourceId);
            return View(acquisitionItem);
        }

        // GET: AcquisitionItems/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AcquisitionItem acquisitionItem = db.AcquisitionItem.Find(id);
            if (acquisitionItem == null)
            {
                return HttpNotFound();
            }
            ViewBag.AcquisitionId = new SelectList(db.Acquisition, "Id", "Id", acquisitionItem.AcquisitionId);
            ViewBag.ResourceId = new SelectList(db.Resource, "Id", "Description", acquisitionItem.ResourceId);
            return View(acquisitionItem);
        }

        // POST: AcquisitionItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AcquisitionId,ResourceId,Active")] AcquisitionItem acquisitionItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(acquisitionItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AcquisitionId = new SelectList(db.Acquisition, "Id", "Id", acquisitionItem.AcquisitionId);
            ViewBag.ResourceId = new SelectList(db.Resource, "Id", "Description", acquisitionItem.ResourceId);
            return View(acquisitionItem);
        }

        // GET: AcquisitionItems/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AcquisitionItem acquisitionItem = db.AcquisitionItem.Find(id);
            if (acquisitionItem == null)
            {
                return HttpNotFound();
            }
            return View(acquisitionItem);
        }

        // POST: AcquisitionItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            AcquisitionItem acquisitionItem = db.AcquisitionItem.Find(id);
            db.AcquisitionItem.Remove(acquisitionItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
