﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ManageResources.Business.Entities;
using ManageResources.Data;

namespace ManageResources.API.Controllers
{
    public class DispatchItemsController : Controller
    {
        private ManageResourcesContext db = new ManageResourcesContext();

        // GET: DispatchItems
        public ActionResult Index()
        {
            var dispatchItem = db.DispatchItem.Include(d => d.Dispatch).Include(d => d.Resource);
            return View(dispatchItem.ToList());
        }

        // GET: DispatchItems/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DispatchItem dispatchItem = db.DispatchItem.Find(id);
            if (dispatchItem == null)
            {
                return HttpNotFound();
            }
            return View(dispatchItem);
        }

        // GET: DispatchItems/Create
        public ActionResult Create()
        {
            ViewBag.DispatchId = new SelectList(db.Dispatch, "Id", "Id");
            ViewBag.ResourceId = new SelectList(db.Resource, "Id", "Description");
            return View();
        }

        // POST: DispatchItems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DispatchId,ResourceId,Quantity,Active")] DispatchItem dispatchItem)
        {
            if (ModelState.IsValid)
            {
                db.DispatchItem.Add(dispatchItem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DispatchId = new SelectList(db.Dispatch, "Id", "Id", dispatchItem.DispatchId);
            ViewBag.ResourceId = new SelectList(db.Resource, "Id", "Description", dispatchItem.ResourceId);
            return View(dispatchItem);
        }

        // GET: DispatchItems/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DispatchItem dispatchItem = db.DispatchItem.Find(id);
            if (dispatchItem == null)
            {
                return HttpNotFound();
            }
            ViewBag.DispatchId = new SelectList(db.Dispatch, "Id", "Id", dispatchItem.DispatchId);
            ViewBag.ResourceId = new SelectList(db.Resource, "Id", "Description", dispatchItem.ResourceId);
            return View(dispatchItem);
        }

        // POST: DispatchItems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DispatchId,ResourceId,Quantity,Active")] DispatchItem dispatchItem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dispatchItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DispatchId = new SelectList(db.Dispatch, "Id", "Id", dispatchItem.DispatchId);
            ViewBag.ResourceId = new SelectList(db.Resource, "Id", "Description", dispatchItem.ResourceId);
            return View(dispatchItem);
        }

        // GET: DispatchItems/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DispatchItem dispatchItem = db.DispatchItem.Find(id);
            if (dispatchItem == null)
            {
                return HttpNotFound();
            }
            return View(dispatchItem);
        }

        // POST: DispatchItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            DispatchItem dispatchItem = db.DispatchItem.Find(id);
            db.DispatchItem.Remove(dispatchItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
