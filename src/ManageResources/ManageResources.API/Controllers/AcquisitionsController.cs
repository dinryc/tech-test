﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ManageResources.Business.Entities;
using ManageResources.Data;

namespace ManageResources.API.Controllers
{
    public class AcquisitionsController : Controller
    {
        private ManageResourcesContext db = new ManageResourcesContext();

        // GET: Acquisitions
        public ActionResult Index()
        {
            var acquisition = db.Acquisition.Include(a => a.Requester);
            return View(acquisition.ToList());
        }

        // GET: Acquisitions/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Acquisition acquisition = db.Acquisition.Find(id);
            if (acquisition == null)
            {
                return HttpNotFound();
            }
            return View(acquisition);
        }

        // GET: Acquisitions/Create
        public ActionResult Create()
        {
            ViewBag.RequesterId = new SelectList(db.Requester, "Id", "Nome");
            return View();
        }

        // POST: Acquisitions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,RequesterId,DateTime,Active")] Acquisition acquisition)
        {
            if (ModelState.IsValid)
            {
                db.Acquisition.Add(acquisition);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RequesterId = new SelectList(db.Requester, "Id", "Nome", acquisition.RequesterId);
            return View(acquisition);
        }

        // GET: Acquisitions/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Acquisition acquisition = db.Acquisition.Find(id);
            if (acquisition == null)
            {
                return HttpNotFound();
            }
            ViewBag.RequesterId = new SelectList(db.Requester, "Id", "Nome", acquisition.RequesterId);
            return View(acquisition);
        }

        // POST: Acquisitions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RequesterId,DateTime,Active")] Acquisition acquisition)
        {
            if (ModelState.IsValid)
            {
                db.Entry(acquisition).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RequesterId = new SelectList(db.Requester, "Id", "Nome", acquisition.RequesterId);
            return View(acquisition);
        }

        // GET: Acquisitions/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Acquisition acquisition = db.Acquisition.Find(id);
            if (acquisition == null)
            {
                return HttpNotFound();
            }
            return View(acquisition);
        }

        // POST: Acquisitions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Acquisition acquisition = db.Acquisition.Find(id);
            db.Acquisition.Remove(acquisition);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
