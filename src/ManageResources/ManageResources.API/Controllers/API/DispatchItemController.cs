﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ManageResources.Business;
using ManageResources.Business.Entities;
using ManageResources.Interfaces.DataServices;
using ManageResources.API.Models;
using Ninject;
using AutoMapper;


namespace ManageResources.API.Controllers.API
{
    public class DispatchItemController : ApiController
    {
        [Inject]
        public IDispatchItemDataService _dispatchItemDataService { get; set; }

        public DispatchItemViewModels Get(long id)
        {
            DispatchItemBusinessService dispatchItemBusinessService = new DispatchItemBusinessService(_dispatchItemDataService);
            DispatchItemViewModels dispatchItem = Mapper.Map<DispatchItem,DispatchItemViewModels>(dispatchItemBusinessService.GetById(id));

            return dispatchItem;
        }

        public List<DispatchItemViewModels> Get()
        {
            DispatchItemBusinessService dispatchItemBusinessService = new DispatchItemBusinessService(_dispatchItemDataService);
            List<DispatchItemViewModels> dispatchItems = Mapper.Map<List<DispatchItem>, List<DispatchItemViewModels>>(dispatchItemBusinessService.GetDispatchItems());

            return dispatchItems;
        }
     
        public HttpResponseMessage Post([FromBody]DispatchItemViewModels dispatchItemVm)
        {
            DispatchItemBusinessService dispatchItemBusinessService = new DispatchItemBusinessService(_dispatchItemDataService);
            DispatchItem dispatchItem = Mapper.Map<DispatchItemViewModels, DispatchItem>(dispatchItemVm);
            dispatchItemBusinessService.CreateDispatchItem(dispatchItem);

            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso inserido com sucesso!"});
        }

        public HttpResponseMessage Put(int id, [FromBody]DispatchItemViewModels dispatchItemVm)
        {
            DispatchItemBusinessService dispatchItemBusinessService = new DispatchItemBusinessService(_dispatchItemDataService);
            DispatchItem dispatchItem = Mapper.Map<DispatchItemViewModels, DispatchItem>(dispatchItemVm);
            dispatchItemBusinessService.UpdateDispatchItem(dispatchItem);
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso alterado com sucesso!" });
        }

        public HttpResponseMessage Delete(int id, [FromBody]DispatchItemViewModels dispatchItemVm)
        {
            DispatchItemBusinessService dispatchItemBusinessService = new DispatchItemBusinessService(_dispatchItemDataService);
            DispatchItem dispatchItem = Mapper.Map<DispatchItemViewModels, DispatchItem>(dispatchItemVm);
            dispatchItemBusinessService.RemoveDispatchItem(dispatchItem);
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso removido com sucesso!" });
        }

    }
}
