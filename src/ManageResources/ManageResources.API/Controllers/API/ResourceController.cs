﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ManageResources.Business;
using ManageResources.Business.Entities;
using ManageResources.Interfaces.DataServices;
using ManageResources.API.Models;
using Ninject;
using AutoMapper;


namespace ManageResources.API.Controllers.API
{
    public class ResourceController : ApiController
    {
        [Inject]
        public IResourceDataService _resourceDataService { get; set; }

        public HttpResponseMessage Get(long id)
        {
            TransactionInfo transaction;
            ResourceBusinessService resourceBusinessService = new ResourceBusinessService(_resourceDataService);
            ResourceViewModels resource = Mapper.Map<Resource,ResourceViewModels>(resourceBusinessService.GetById(id, out transaction));

            if (!transaction.ReturnStatus)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { returnStatus = transaction.ReturnStatus, returnMessage = transaction.ReturnMessage, errors = transaction.ValidationErrors });
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { resource = resource, returnStatus = transaction.ReturnStatus, returnMessage = transaction.ReturnMessage }); ;
        }

        public HttpResponseMessage Get()
        {
            TransactionInfo transaction;
            ResourceBusinessService resourceBusinessService = new ResourceBusinessService(_resourceDataService);
            List<ResourceViewModels> resources = Mapper.Map<List<Resource>, List<ResourceViewModels>>(resourceBusinessService.GetResources(out transaction));

            if (!transaction.ReturnStatus)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { returnStatus = transaction.ReturnStatus, returnMessage = transaction.ReturnMessage, errors = transaction.ValidationErrors });
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { resources = resources, transaction = transaction}); ;
        }
     
        public HttpResponseMessage Post([FromBody]ResourceViewModels resourceVm)
        {
            TransactionInfo transaction;
            ResourceBusinessService resourceBusinessService = new ResourceBusinessService(_resourceDataService);
            Resource resource = Mapper.Map<ResourceViewModels, Resource>(resourceVm);
            resourceBusinessService.CreateResource(resource, out transaction);

            if (!transaction.ReturnStatus)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { returnStatus = transaction.ReturnStatus, returnMessage = transaction.ReturnMessage, errors = transaction.ValidationErrors});
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { resourceId = resource.Id, returnStatus = transaction.ReturnStatus, returnMessage = transaction.ReturnMessage });
        }

        public HttpResponseMessage Put(int id, [FromBody]ResourceViewModels resourceVm)
        {
            TransactionInfo transaction;
            resourceVm.Id = id;
            ResourceBusinessService resourceBusinessService = new ResourceBusinessService(_resourceDataService);
            Resource resource = Mapper.Map<ResourceViewModels, Resource>(resourceVm);
            resourceBusinessService.UpdateResource(resource, out transaction);

            if (!transaction.ReturnStatus)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { returnStatus = transaction.ReturnStatus, returnMessage = transaction.ReturnMessage, errors = transaction.ValidationErrors });
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { resourceId = resource.Id, returnStatus = transaction.ReturnStatus, returnMessage = transaction.ReturnMessage }); ;

        }

        public HttpResponseMessage Delete(long id)
        {
            TransactionInfo transaction;
            ResourceBusinessService resourceBusinessService = new ResourceBusinessService(_resourceDataService);
            
            resourceBusinessService.RemoveResource(id, out transaction);

            if (!transaction.ReturnStatus)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { returnStatus = transaction.ReturnStatus, returnMessage = transaction.ReturnMessage, errors = transaction.ValidationErrors });
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { returnStatus = transaction.ReturnStatus, returnMessage = transaction.ReturnMessage }); ;
        }
    }
}
