﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ManageResources.Business;
using ManageResources.Business.Entities;
using ManageResources.Interfaces.DataServices;
using ManageResources.API.Models;
using Ninject;
using AutoMapper;


namespace ManageResources.API.Controllers.API
{
    public class DispatchController : ApiController
    {
        [Inject]
        public IDispatchDataService _dispatchDataService { get; set; }

        public DispatchViewModels Get(long id)
        {
            DispatchBusinessService dispatchBusinessService = new DispatchBusinessService(_dispatchDataService);
            DispatchViewModels dispatch = Mapper.Map<Dispatch,DispatchViewModels>(dispatchBusinessService.GetById(id));

            return dispatch;
        }

        public List<DispatchViewModels> Get()
        {
            DispatchBusinessService dispatchBusinessService = new DispatchBusinessService(_dispatchDataService);
            List<DispatchViewModels> dispatches = Mapper.Map<List<Dispatch>, List<DispatchViewModels>>(dispatchBusinessService.GetDispatches());

            return dispatches;
        }
     
        public HttpResponseMessage Post([FromBody]DispatchViewModels dispatchVm)
        {
            DispatchBusinessService dispatchBusinessService = new DispatchBusinessService(_dispatchDataService);
            Dispatch dispatch = Mapper.Map<DispatchViewModels, Dispatch>(dispatchVm);
            dispatchBusinessService.CreateDispatch(dispatch);

            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso inserido com sucesso!"});
        }

        public HttpResponseMessage Put(int id, [FromBody]DispatchViewModels dispatchVm)
        {
            DispatchBusinessService dispatchBusinessService = new DispatchBusinessService(_dispatchDataService);
            Dispatch dispatch = Mapper.Map<DispatchViewModels, Dispatch>(dispatchVm);
            dispatchBusinessService.UpdateDispatch(dispatch);
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso alterado com sucesso!" });
        }

        public HttpResponseMessage Delete(int id, [FromBody]DispatchViewModels dispatchVm)
        {
            DispatchBusinessService dispatchBusinessService = new DispatchBusinessService(_dispatchDataService);
            Dispatch dispatch = Mapper.Map<DispatchViewModels, Dispatch>(dispatchVm);
            dispatchBusinessService.RemoveDispatch(dispatch);
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso removido com sucesso!" });
        }

    }
}
