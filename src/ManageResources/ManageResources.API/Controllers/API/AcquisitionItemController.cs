﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ManageResources.Business;
using ManageResources.Business.Entities;
using ManageResources.Interfaces.DataServices;
using ManageResources.API.Models;
using Ninject;
using AutoMapper;


namespace ManageResources.API.Controllers.API
{
    public class AcquisitionItemController : ApiController
    {
        [Inject]
        public IAcquisitionItemDataService _acquisitionItemDataService { get; set; }

        public AcquisitionItemViewModels Get(long id)
        {
            AcquisitionItemBusinessService acquisitionItemBusinessService = new AcquisitionItemBusinessService(_acquisitionItemDataService);
            AcquisitionItemViewModels acquisitionItem = Mapper.Map<AcquisitionItem,AcquisitionItemViewModels>(acquisitionItemBusinessService.GetById(id));

            return acquisitionItem;
        }

        public List<AcquisitionItemViewModels> Get()
        {
            AcquisitionItemBusinessService acquisitionItemBusinessService = new AcquisitionItemBusinessService(_acquisitionItemDataService);
            List<AcquisitionItemViewModels> acquisitionItems = Mapper.Map<List<AcquisitionItem>, List<AcquisitionItemViewModels>>(acquisitionItemBusinessService.GetAcquisitionItems());

            return acquisitionItems;
        }
     
        public HttpResponseMessage Post([FromBody]AcquisitionItemViewModels acquisitionItemVm)
        {
            AcquisitionItemBusinessService acquisitionItemBusinessService = new AcquisitionItemBusinessService(_acquisitionItemDataService);
            AcquisitionItem acquisitionItem = Mapper.Map<AcquisitionItemViewModels, AcquisitionItem>(acquisitionItemVm);
            acquisitionItemBusinessService.CreateAcquisitionItem(acquisitionItem);

            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso inserido com sucesso!"});
        }

        public HttpResponseMessage Put(int id, [FromBody]AcquisitionItemViewModels acquisitionItemVm)
        {
            AcquisitionItemBusinessService acquisitionItemBusinessService = new AcquisitionItemBusinessService(_acquisitionItemDataService);
            AcquisitionItem acquisitionItem = Mapper.Map<AcquisitionItemViewModels, AcquisitionItem>(acquisitionItemVm);
            acquisitionItemBusinessService.UpdateAcquisitionItem(acquisitionItem);
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso alterado com sucesso!" });
        }

        public HttpResponseMessage Delete(int id, [FromBody]AcquisitionItemViewModels acquisitionItemVm)
        {
            AcquisitionItemBusinessService acquisitionItemBusinessService = new AcquisitionItemBusinessService(_acquisitionItemDataService);
            AcquisitionItem acquisitionItem = Mapper.Map<AcquisitionItemViewModels, AcquisitionItem>(acquisitionItemVm);
            acquisitionItemBusinessService.RemoveAcquisitionItem(acquisitionItem);
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso removido com sucesso!" });
        }

    }
}
