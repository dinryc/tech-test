﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ManageResources.Business;
using ManageResources.Business.Entities;
using ManageResources.Interfaces.DataServices;
using ManageResources.API.Models;
using Ninject;
using AutoMapper;


namespace ManageResources.API.Controllers.API
{
    public class RequesterController : ApiController
    {
        [Inject]
        public IRequesterDataService _requesterDataService { get; set; }

        public RequesterViewModels Get(long id)
        {
            RequesterBusinessService requesterBusinessService = new RequesterBusinessService(_requesterDataService);
            RequesterViewModels requester = Mapper.Map<Requester,RequesterViewModels>(requesterBusinessService.GetById(id));

            return requester;
        }

        public List<RequesterViewModels> Get()
        {
            RequesterBusinessService requesterBusinessService = new RequesterBusinessService(_requesterDataService);
            List<RequesterViewModels> requesters = Mapper.Map<List<Requester>, List<RequesterViewModels>>(requesterBusinessService.GetRequesters());

            return requesters;
        }
     
        public HttpResponseMessage Post([FromBody]RequesterViewModels requesterVm)
        {
            RequesterBusinessService requesterBusinessService = new RequesterBusinessService(_requesterDataService);
            Requester requester = Mapper.Map<RequesterViewModels, Requester>(requesterVm);
            requesterBusinessService.CreateRequester(requester);

            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso inserido com sucesso!"});
        }

        public HttpResponseMessage Put(int id, [FromBody]RequesterViewModels requesterVm)
        {
            RequesterBusinessService requesterBusinessService = new RequesterBusinessService(_requesterDataService);
            Requester requester = Mapper.Map<RequesterViewModels, Requester>(requesterVm);
            requesterBusinessService.UpdateRequester(requester);
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso alterado com sucesso!" });
        }

        public HttpResponseMessage Delete(int id, [FromBody]RequesterViewModels requesterVm)
        {
            RequesterBusinessService requesterBusinessService = new RequesterBusinessService(_requesterDataService);
            Requester requester = Mapper.Map<RequesterViewModels, Requester>(requesterVm);
            requesterBusinessService.RemoveRequester(requester);
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso removido com sucesso!" });
        }

    }
}
