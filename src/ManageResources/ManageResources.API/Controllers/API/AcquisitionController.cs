﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ManageResources.Business;
using ManageResources.Business.Entities;
using ManageResources.Interfaces.DataServices;
using ManageResources.API.Models;
using Ninject;
using AutoMapper;


namespace ManageResources.API.Controllers.API
{
    public class AcquisitionController : ApiController
    {
        [Inject]
        public IAcquisitionDataService _acquisitionDataService { get; set; }

        public AcquisitionViewModels Get(long id)
        {
            AcquisitionBusinessService acquisitionBusinessService = new AcquisitionBusinessService(_acquisitionDataService);
            AcquisitionViewModels acquisition = Mapper.Map<Acquisition,AcquisitionViewModels>(acquisitionBusinessService.GetById(id));

            return acquisition;
        }

        public List<AcquisitionViewModels> Get()
        {
            AcquisitionBusinessService acquisitionBusinessService = new AcquisitionBusinessService(_acquisitionDataService);
            List<AcquisitionViewModels> acquisitions = Mapper.Map<List<Acquisition>, List<AcquisitionViewModels>>(acquisitionBusinessService.GetAcquisitions());

            return acquisitions;
        }
     
        public HttpResponseMessage Post([FromBody]AcquisitionViewModels acquisitionVm)
        {
            AcquisitionBusinessService acquisitionBusinessService = new AcquisitionBusinessService(_acquisitionDataService);
            Acquisition acquisition = Mapper.Map<AcquisitionViewModels, Acquisition>(acquisitionVm);
            acquisitionBusinessService.CreateAcquisition(acquisition);

            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso inserido com sucesso!"});
        }

        public HttpResponseMessage Put(int id, [FromBody]AcquisitionViewModels acquisitionVm)
        {
            AcquisitionBusinessService acquisitionBusinessService = new AcquisitionBusinessService(_acquisitionDataService);
            Acquisition acquisition = Mapper.Map<AcquisitionViewModels, Acquisition>(acquisitionVm);
            acquisitionBusinessService.UpdateAcquisition(acquisition);
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso alterado com sucesso!" });
        }

        public HttpResponseMessage Delete(int id, [FromBody]AcquisitionViewModels acquisitionVm)
        {
            AcquisitionBusinessService acquisitionBusinessService = new AcquisitionBusinessService(_acquisitionDataService);
            Acquisition acquisition = Mapper.Map<AcquisitionViewModels, Acquisition>(acquisitionVm);
            acquisitionBusinessService.RemoveAcquisition(acquisition);
            return Request.CreateResponse(HttpStatusCode.OK, new { message = "Recurso removido com sucesso!" });
        }

    }
}
