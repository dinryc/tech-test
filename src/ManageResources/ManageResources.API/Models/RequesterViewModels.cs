﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ManageResources.Business.Entities;

namespace ManageResources.API.Models
{
    public class RequesterViewModels : EntityBase
    {
        public string Nome { get; set; }
        public string Sobrenome { get; set; }
    }
}