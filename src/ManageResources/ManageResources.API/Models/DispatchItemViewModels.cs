﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ManageResources.Business.Entities;

namespace ManageResources.API.Models
{
    public class DispatchItemViewModels : EntityBase
    {
        public long DispatchId { get; set; }
        public long ResourceId { get; set; }
        public double Quantity { get; set; }
    }
}