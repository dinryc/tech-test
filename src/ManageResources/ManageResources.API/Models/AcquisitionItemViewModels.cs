﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ManageResources.Business.Entities;

namespace ManageResources.API.Models
{
    public class AcquisitionItemViewModels : EntityBase
    {
        public long AcquisitionId { get; set; }
        public long ResourceId { get; set; }
        public double Quantity { get; set; }
    }
}