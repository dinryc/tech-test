﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ManageResources.Business.Entities;

namespace ManageResources.API.Models
{
    public class ResourceViewModels:EntityBase
    {        
        public string Description { get; set; }
        public double Quantity { get; set; }
        public string Note { get; set; }
    }
}