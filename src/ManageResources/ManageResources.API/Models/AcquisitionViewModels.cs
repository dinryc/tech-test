﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ManageResources.Business.Entities;

namespace ManageResources.API.Models
{
    public class AcquisitionViewModels : EntityBase
    {
        public long RequesterId { get; set; }
        public DateTime DateTime { get; set; }
    }
}