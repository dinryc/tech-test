﻿using AutoMapper;

namespace ManageResources.API.AutoMapper
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<EntityToViewModelMappingProfile>();
                x.AddProfile<ViewModelToEntityMappingProfile>();
            });
        }
    }
}