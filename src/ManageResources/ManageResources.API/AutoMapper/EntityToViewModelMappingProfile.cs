﻿using AutoMapper;
using ManageResources.Business.Entities;
using ManageResources.API.Models;

namespace ManageResources.API.AutoMapper
{
    class EntityToViewModelMappingProfile : Profile
    {
        public new string ProfileName
        {
            get { return "EntityToViewModelMappings"; }
        }
        public EntityToViewModelMappingProfile()
        {
            CreateMap<Resource,        ResourceViewModels>();
            CreateMap<Requester,       RequesterViewModels>();
            CreateMap<Acquisition,     AcquisitionViewModels>();
            CreateMap<AcquisitionItem, AcquisitionItemViewModels>();
            CreateMap<Dispatch,        DispatchViewModels>();
            CreateMap<DispatchItem,    DispatchItemViewModels>();
        }
    }
}