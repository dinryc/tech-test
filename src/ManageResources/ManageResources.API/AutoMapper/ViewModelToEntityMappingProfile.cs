﻿using AutoMapper;
using ManageResources.Business.Entities;
using ManageResources.API.Models;

namespace ManageResources.API.AutoMapper
{
    public class ViewModelToEntityMappingProfile : Profile
    {
        public new string ProfileName
        {
            get { return "ViewModelToEntityMappings"; }
        }
        public ViewModelToEntityMappingProfile()
        {
            CreateMap<ResourceViewModels, Resource>();
            CreateMap<RequesterViewModels, Requester > ();
            CreateMap<AcquisitionViewModels, Acquisition > ();
            CreateMap<AcquisitionItemViewModels, AcquisitionItem > ();
            CreateMap<DispatchViewModels, Dispatch > ();
            CreateMap<DispatchItemViewModels, DispatchItem > ();
        }
    }
}